<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="../AboutWindow.ui" line="0"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../AboutWindow.ui" line="0"/>
        <source>A graphical Program to create and edit Appstream files</source>
        <translation>Ein grafisches Programm zum erstellen und bearbeiten von AppStream Dateien</translation>
    </message>
    <message>
        <location filename="../AboutWindow.ui" line="0"/>
        <source>This Program is licensed under GPL 3</source>
        <translation>Dieses Programm ist unter der GPL 3 lizenziert</translation>
    </message>
    <message>
        <location filename="../AboutWindow.ui" line="0"/>
        <source>View source</source>
        <translation>Quelltext anzeigen</translation>
    </message>
    <message>
        <location filename="../AboutWindow.ui" line="0"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>AdvancedWidget</name>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.py" line="204"/>
        <location filename="../AdvancedWidget.py" line="177"/>
        <location filename="../AdvancedWidget.py" line="68"/>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="82"/>
        <source>New Suggestion</source>
        <translation>Neuer Vorschlag</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="82"/>
        <source>Please enter a new ID</source>
        <translation>Bitte gib eine ID ein</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="141"/>
        <location filename="../AdvancedWidget.py" line="127"/>
        <location filename="../AdvancedWidget.py" line="100"/>
        <location filename="../AdvancedWidget.py" line="86"/>
        <source>ID in List</source>
        <translation>ID in der Liste</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="141"/>
        <location filename="../AdvancedWidget.py" line="127"/>
        <location filename="../AdvancedWidget.py" line="100"/>
        <location filename="../AdvancedWidget.py" line="86"/>
        <source>This ID is already in the List</source>
        <translation>Diese ID ist bereits in der Liste</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="96"/>
        <source>Edit Suggestion</source>
        <translation>Vorschlag bearbeiten</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="137"/>
        <location filename="../AdvancedWidget.py" line="96"/>
        <source>Please edit the ID</source>
        <translation>Bitte bearbeite die ID</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="123"/>
        <source>New Replacement</source>
        <translation>Neuer Ersatz</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="123"/>
        <source>Please enter a ID</source>
        <translation>Bitte gib eine ID ein</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.py" line="137"/>
        <source>Edit Replacement</source>
        <translation>Ersatz bearbeiten</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Translation</source>
        <translation>Übersetzung</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>You can manage the translations here. For more information take a look at the documentation.</source>
        <translation>Die kannst hier die Übersetzungen verwalten. Für mehr Informationen wirf einen Blick in die Dokumentation.</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Domain</source>
        <translation>Domain</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Suggests</source>
        <translation>Vorschläge</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>If your Sofware suggests other Software, you can add the IDs here</source>
        <translation>Wenn deine Software eine andere vorschlägt, kannst du hier die ID einfügen</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Replaces</source>
        <translation>Ersetzt</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>If the ID of your Software has changed, you add the old IDs here</source>
        <translation>Wenn sich die ID deiner Software geändert hat, kannst du die alten IDs hier angeben</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Tags</source>
        <translation>Tags</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>You can edit the tags here. For more information take a look at the documentation.</source>
        <translation>Du kannst hier die Tags bearbeiten.  Für mehr Informationen wirf einen Blick in die Dokumentation.</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Namespace</source>
        <translation>Namensraum</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Custom</source>
        <translation>Eigen</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>You can add custom values here. For more information take a look at the documentation.</source>
        <translation type="unfinished">Du kannst hier eigene Werte eintragen. Für mehr Informationen wirf einen Blick in die Dokumentation.</translation>
    </message>
    <message>
        <location filename="../AdvancedWidget.ui" line="0"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
</context>
<context>
    <name>ArtifactWindow</name>
    <message>
        <location filename="../ArtifactWindow.py" line="24"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="35"/>
        <source>No URL</source>
        <translation>Keine URL</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="35"/>
        <source>Please enter a URL</source>
        <translation>Bitte gib eine gültige URL an</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="67"/>
        <location filename="../ArtifactWindow.py" line="64"/>
        <location filename="../ArtifactWindow.py" line="38"/>
        <source>Invalid URL</source>
        <translation>Ungültige URL</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="38"/>
        <source>Please enter a valid URL</source>
        <translation>Bitte gib eine gültige URL an</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="46"/>
        <source>No checksums</source>
        <translation>Keine Checksumme</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="46"/>
        <source>You need at least one checksum</source>
        <translation>Es muss mindestens eine Checksumme angegeben werden</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="67"/>
        <location filename="../ArtifactWindow.py" line="64"/>
        <source>Can&apos;t get the File from the URL</source>
        <translation>Kann URL nicht aufrufen</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="70"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="70"/>
        <source>A Error happened while calculatig the checksum</source>
        <translation>Beim Berechnen der Checksummen ist ein fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="166"/>
        <source>Edit Artifact</source>
        <translation>Artifakt bearbeiten</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.py" line="170"/>
        <source>Add Artifact</source>
        <translation>Artifakt hinzufügen</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Source</source>
        <translation>Quelltext</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Binary</source>
        <translation>Binärdatei</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Platform:</source>
        <translation>Platform:</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Checksums</source>
        <translation>Checksumme</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Calculate Checksums</source>
        <translation>Checksumme berechnen</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Installed:</source>
        <translation>Installiert:</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Download:</source>
        <translation>Download:</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Filename:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>You can specify a optional Filename, under which the File should be saved here</source>
        <translation>Du kannst hier einen optionalen Namen, unter welchen die Datei gepeichertt werden soll, angeben</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ArtifactWindow.ui" line="0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DescriptionWidget</name>
    <message>
        <location filename="../DescriptionWidget.py" line="67"/>
        <location filename="../DescriptionWidget.py" line="24"/>
        <source>Translate</source>
        <translation>Übersetzen</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="64"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="65"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.ui" line="0"/>
        <location filename="../DescriptionWidget.py" line="218"/>
        <location filename="../DescriptionWidget.py" line="196"/>
        <location filename="../DescriptionWidget.py" line="66"/>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="120"/>
        <source>Add Item</source>
        <translation>Item hinzufügen</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="120"/>
        <source>Please enter a new list item</source>
        <translation>Bitte gebe den text für das neue Item ein</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="133"/>
        <source>Edit Item</source>
        <translation>Item bearbeiten</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="133"/>
        <source>Please enter a new value for the Item</source>
        <translation>Bitte gib einen neuen text für das Item ein</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="190"/>
        <source>Paragraph</source>
        <translation>Paragraph</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="210"/>
        <source>Ordered List</source>
        <translation>Geordnete Liste</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.py" line="212"/>
        <source>Unordered List</source>
        <translation>Ungeordnete Liste</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.ui" line="0"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.ui" line="0"/>
        <source>Content</source>
        <translation>Inhalt</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.ui" line="0"/>
        <source>This Textbox shows a Preview of the Description</source>
        <translation>Diese Textbox zeigt eine Vorschau der Beschreibung</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.ui" line="0"/>
        <source>Add Paragraph</source>
        <translation>Paragraph hinzufügen</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.ui" line="0"/>
        <source>Add Ordered List</source>
        <translation>Geordnete Liste hinzufügen</translation>
    </message>
    <message>
        <location filename="../DescriptionWidget.ui" line="0"/>
        <source>Add Unordered List</source>
        <translation>Ungeordnete Liste hinzufügen</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.py" line="68"/>
        <source>Not specified</source>
        <translation>Nicht angegeben</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="69"/>
        <source>Required</source>
        <translation>Benötigt</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="70"/>
        <source>Recommend</source>
        <translation>Empfohlen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="71"/>
        <source>Supported</source>
        <translation>Unterstützt</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="87"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="88"/>
        <source>Console</source>
        <translation>Kommandzeile</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="89"/>
        <source>Web Application</source>
        <translation>Webanwendung</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="90"/>
        <source>Service</source>
        <translation>Service</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="91"/>
        <source>Addon</source>
        <translation>Addon</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="92"/>
        <source>Font</source>
        <translation>Schriftart</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="93"/>
        <source>Icon Theme</source>
        <translation>Icontheme</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="94"/>
        <source>Codecs</source>
        <translation>Codec</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="95"/>
        <source>Input Method</source>
        <translation>Eingabemethode</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="96"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="107"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="199"/>
        <source>Untitled</source>
        <translation>Unbenannt</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="779"/>
        <location filename="../MainWindow.py" line="755"/>
        <location filename="../MainWindow.py" line="205"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="216"/>
        <source>No templates found</source>
        <translation>Keine Vorlagen gefunden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="230"/>
        <source>No recent files</source>
        <translation>Keine zuletzt geöffneten Dateien</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="243"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="260"/>
        <source>Unsaved changes</source>
        <translation>Nicht gespeicherte Änderungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="260"/>
        <source>You have unsaved changes. Do you want to save now?</source>
        <translation>Du hast nicht gespeicherte Änderungen. Möchtest du sie jetzt speichern?</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="271"/>
        <source>Welcome to jdAppdataEdit!</source>
        <translation>Willkommen bei jdAppdataEdit!</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="272"/>
        <source>With jdAppdataEdit you can create and edit AppStream files (*.metainfo.xml or .appdata.xml). This files are to provide data for your Application (Description, Screenshots etc.) to Software Centers.</source>
        <translation>Mit jdAppdataEdit kannst du AppStream Dateien (*.metainfo.xml oder *.appdata.xml) erstellen und bearbeiten. Diese Dateien stellen Informationen (z.B. Beschreibung, Screeenshots usw.) für Software Center zur Verfügung.</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="273"/>
        <source>It is highly recommend to read the the AppStream Documentation before using this Program. You can open it under ?&gt;AppStream documentation.</source>
        <translation>Es wird empfohlen vor der benutzung die AppStream Dokumentation zu lesen. Du findest sie unter ?&gt;AppStream Dokumentation.</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="274"/>
        <source>You can check if your AppStream is valid under Tools&gt;Validate.</source>
        <translation>Du kannst deine AppStream Datei unter Werkzeuge&gt;Validieren auf Gültigkeit überprüfen.</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="277"/>
        <source>Show this dialog at startup</source>
        <translation>Diesen Dialog beim Programmstart anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="281"/>
        <source>Welcome</source>
        <translation>Willkommen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="302"/>
        <source>desktop-entry-lib not found</source>
        <translation>desktop-entrylib nicht gefunden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="302"/>
        <source>This function needs the desktop-entry-lib python module to work</source>
        <translation>Diese Funktion benötigt das desktop-entry-lib Python Modul</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="310"/>
        <source>Desktop Entry Files</source>
        <translation>Desktopeinträge</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="400"/>
        <location filename="../MainWindow.py" line="365"/>
        <location filename="../MainWindow.py" line="310"/>
        <source>All Files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="400"/>
        <location filename="../MainWindow.py" line="365"/>
        <source>AppStream Files</source>
        <translation>AppStream Dateien</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="385"/>
        <source>Enter URL</source>
        <translation>URL eingeben</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="385"/>
        <source>Please enter a URL</source>
        <translation>Bitte gib eine URL ein</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.py" line="516"/>
        <location filename="../MainWindow.py" line="436"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.py" line="662"/>
        <location filename="../MainWindow.py" line="520"/>
        <location filename="../MainWindow.py" line="440"/>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="770"/>
        <location filename="../MainWindow.py" line="603"/>
        <location filename="../MainWindow.py" line="447"/>
        <source>Invalid URL</source>
        <translation>Ungültige URL</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="447"/>
        <source>The URL {{url}} does not work</source>
        <translation>Die URL {{url}} funktioniert nicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="605"/>
        <location filename="../MainWindow.py" line="449"/>
        <source>Everything OK</source>
        <translation>Alles in Ordnung</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="605"/>
        <location filename="../MainWindow.py" line="449"/>
        <source>All URLs are working</source>
        <translation>Alle URLs funktionieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="510"/>
        <source>Stable</source>
        <translation>Stabil</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="511"/>
        <source>Development</source>
        <translation>Entwicklung</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="545"/>
        <source>packaging not found</source>
        <translation>packaging nicht gefunden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="545"/>
        <source>This function needs the packaging python module to work</source>
        <translation>Diese Funktion benötigt das packaging Python Modul</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="560"/>
        <source>Could not parse version</source>
        <translation>Konnte Version nicht auswerten</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="560"/>
        <source>Coul not parse version {{version}}</source>
        <translation>Konnte die Version {{version}} nicht auswerten</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="587"/>
        <source>Overwrite evrything</source>
        <translation>Alles überschreiben</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="587"/>
        <source>If you proceed, all your chnages in the release tab will be overwritten. Continue?</source>
        <translation>Wenn du forfährst wird der Release Tab überschrieben. Fortfahren?</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="603"/>
        <source>The URL {url} does not work</source>
        <translation>Die URL {url} funktioniert nicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="616"/>
        <source>Add a Categorie</source>
        <translation>Kategorie hinzufügen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="616"/>
        <source>Please select a Categorie from the list below</source>
        <translation>Bitte wähle eine Kategorie aus</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="620"/>
        <source>Categorie already added</source>
        <translation>Kategorie bereits vorhanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="620"/>
        <source>You can&apos;t add the same Categorie twice</source>
        <translation>Du kannst dieselbe Kategorie nicht ein zweites Mal hinzufügen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="678"/>
        <source>New Keyword</source>
        <translation>Neues Schlüsselwort</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="678"/>
        <source>Please enter a new Keyword</source>
        <translation>Bitte gib ein neues Schlüsselwort ein</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="696"/>
        <location filename="../MainWindow.py" line="682"/>
        <source>Keyword in List</source>
        <translation>Schlüsselwort in Liste</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="696"/>
        <location filename="../MainWindow.py" line="682"/>
        <source>This Keyword is already in the List</source>
        <translation>ADs Schlüsselwort befindet sich bereits in der Liste</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="692"/>
        <source>Edit Keyword</source>
        <translation>Schlüsselwort bearbeiten</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="692"/>
        <source>Please edit the Keyword</source>
        <translation>Bitte bearbeite das Schlüsselwort</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="752"/>
        <source>File not found</source>
        <translation>datei nicht gefunden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="752"/>
        <source>{{path}} does not exists</source>
        <translation>{{path}} existiert nicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="755"/>
        <source>An error occurred while trying to open {{path}}</source>
        <translation>Während des öffnens von {{path}} ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="770"/>
        <source>{{url}} is not a valid http/https URL</source>
        <translation>{{url}} ist keine gültige http/https URL</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="776"/>
        <source>Could not connect</source>
        <translation>Kann nicht verbinden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="776"/>
        <source>Could not connect to {{url}}</source>
        <translation>Es kann keine Verbindung zu {{url}} aufgebaut werden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="779"/>
        <source>An error occurred while trying to connect to {{url}}</source>
        <translation>Während des verbindens zu {{url}} ist ein fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="826"/>
        <source>XML parsing failed</source>
        <translation>XML parsen fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="830"/>
        <source>No component tag</source>
        <translation>Kein component Tag</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="830"/>
        <source>This XML file has no component tag</source>
        <translation>Die XML Datei hat keinen component Tag</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="833"/>
        <source>Too many component tags</source>
        <translation>Zu viele components Tags</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="833"/>
        <source>Only files with one component tag are supported</source>
        <translation>Nur ein component Tag ist erlaubt</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="1145"/>
        <source>No ID</source>
        <translation>Keine ID</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="1145"/>
        <source>You need to set a ID to use this feature</source>
        <translation>Du benötigst eine ID, um dieses Feature nutzen zu können</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="1164"/>
        <source>{{binary}} not found</source>
        <translation>{{binary}} nicht gefunden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="1164"/>
        <source>{{binary}} was not found. Make sure it is installed and in PATH.</source>
        <translation>{{binary}} wurde nicht gefunden. Stelle sicher, dass es installiert und im PATH ist.</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>The uniquely ID of the Software</source>
        <translation>Die eindeutige ID der Software</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>A human-readable name for your Software</source>
        <translation>Der Name der Software</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Translate</source>
        <translation>Übersetzen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Summary:</source>
        <translation>Zusammenfassung:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>A short summary of your Software</source>
        <translation>Eine kurze Beschreibung der Software</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Developer Name:</source>
        <translation>Entwicklername:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>The Name of the Developer</source>
        <translation>Der Name des Entwicklers der Software</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Desktop File:</source>
        <translation>Desktopdatei:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>The Name of the .desktop file of your Software</source>
        <translation>Der Name der .desktop Datei der Software</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Metaddata License:</source>
        <translation>Metadatenlizenz:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Project License:</source>
        <translation>Projektlizenz:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Update Contact:</source>
        <translation>Updatekontakt:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>You can provide a email address for contacting you here</source>
        <translation>Du kannst hier eine E-Mail Adresse für die Kontaktaufnahme angeben</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Project Group:</source>
        <translation>Projektgruppe:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>If the software is part of a bigger group e.g. GNOME or KDE, you can provide it here</source>
        <translation>Wenn die Software teil einer größeren Gruppe z.B. GNOME oder KDE ist, kannst du das hier angeben</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Screenshots</source>
        <translation>Screenshots</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Check Image URLs</source>
        <translation>Bild URLs testen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Releases</source>
        <translation>Veröffentlichungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Sort</source>
        <translation>Sortieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Links</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Homepage:</source>
        <translation>Homepage:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Should be a link to the upstream homepage for the component</source>
        <translation>Sollte ein Link auf die Homepage der Komponente sein</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Bugtracker:</source>
        <translation>Bugtracker:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Should point to the software&apos;s bug tracking system, for users to report new bugs</source>
        <translation>Sollte auf das Fehlerverfolgungssystem der Software verweisen, damit die Benutzer neue Fehler melden können</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>FAQ:</source>
        <translation>FAQ:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Should link a FAQ page for this software, to answer some of the most-asked questions in detail</source>
        <translation>Sollte eine FAQ-Seite für diese Software verlinken, um einige der am häufigsten gestellten Fragen ausführlich zu beantworten</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Help:</source>
        <translation>Hilfe:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Should provide a web link to an online user&apos;s reference, a software manual or help page</source>
        <translation>Sollte einen Weblink zu einer Online-Benutzerreferenz, einem Software-Handbuch oder einer Hilfeseite enthalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Donation:</source>
        <translation>Spenden:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>URLs of this type should point to a webpage showing information on how to donate to the described software project</source>
        <translation>URLs dieser Art sollten auf eine Webseite verweisen, die Informationen darüber enthält, wie man für das beschriebene Softwareprojekt spenden kann</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Translate:</source>
        <translation>Übersetzen:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>URLs of this type should point to a webpage where users can submit or modify translations of the upstream project</source>
        <translation>URLs dieser Art sollten auf eine Webseite verweisen, auf der Benutzer Übersetzungen des Upstream-Projekts einreichen oder ändern können</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Contact:</source>
        <translation>Kontakt:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>URLs of this type should allow the user to contact the developer</source>
        <translation>URLs dieser Art sollten es dem Benutzer ermöglichen, den Entwickler zu kontaktieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Sourcecode:</source>
        <translation>Quelltext:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>URLs of this type should point to a webpage on which the user can browse the sourcecode</source>
        <translation>URLs dieser Art sollten auf eine Webseite verweisen, auf der der Benutzer den Quellcode ansehen kann</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Contribute:</source>
        <translation>Beitragen:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>URLs of this type should point to a webpage showing information on how to contribute to the described software project</source>
        <translation>URLs dieser Art sollten auf eine Webseite verweisen, die Informationen darüber enthält, wie man zu dem beschriebenen Softwareprojekt beitragen kann</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Check URLs</source>
        <translation>URLs überpüfen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Categories</source>
        <translation>Kategorien</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Control</source>
        <translation>Eingabe</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>You can select  here, how your Software can be controlled</source>
        <translation>Du kannst hier die Eingabemöglichkeiten der Software auswählen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Mouse:</source>
        <translation>Maus:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Keyboard:</source>
        <translation>Tastatur:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Command Line:</source>
        <translation>Kommandozeile:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Graphics Tablet:</source>
        <translation>Grafiktablet:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Touchscreen:</source>
        <translation>Touchscreen:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Gamepad:</source>
        <translation>Gamepad:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>TV remote:</source>
        <translation>Fernbedienung:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Voice:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Computer vision:</source>
        <translation>Computer Vision:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Relations</source>
        <translation>Relationen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>OARS</source>
        <translation>OARS</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Provides</source>
        <translation>Bietet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>This is for advanced users. For more information take a look at the Appstream documentation.</source>
        <translation>Das ist für fortgeschrittene Nutzer. Für weitere Informationen besuche die Appstream Dokumentation.</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Keywords</source>
        <translation>Schlüsselwörter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>You can manage the Keywords for the Software here</source>
        <translation>Die kannst hier die Schlüsselwörter der Software verwalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Advanced</source>
        <translation>Erweitert</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open recent</source>
        <translation>Zuletzt göffnete Dateien</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>New (from Template)</source>
        <translation>Neu (von Vorlage)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Tools</source>
        <translation>Werkzeuge</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Save As...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Validate</source>
        <translation>Validieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>AppStream documentation</source>
        <translation>AppStream Dokumentation</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>View XML</source>
        <translation>XML anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Preview in Gnome Software</source>
        <translation>Vorschau in Gnome Software</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Show welcome dialog</source>
        <translation>Willkommensdialog anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open from URL</source>
        <translation>Von URL öffnen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>New (from .desktop file)</source>
        <translation>Neu (von .desktop Datei)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Manage templates</source>
        <translation>Vorlagen verwalten</translation>
    </message>
</context>
<context>
    <name>ManageTemplatesWindow</name>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="58"/>
        <location filename="../ManageTemplatesWindow.py" line="42"/>
        <source>Enter name</source>
        <translation>Name eingeben</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="42"/>
        <source>This will save your current document as template. Please enter a name.</source>
        <translation>Dies speichert dein aktuelles Dokument als Vorlage. Bitte gib einen namen ein.</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="64"/>
        <location filename="../ManageTemplatesWindow.py" line="48"/>
        <source>Name exists</source>
        <translation>Name existiert</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="64"/>
        <location filename="../ManageTemplatesWindow.py" line="48"/>
        <source>There is already a template with this name</source>
        <translation>Es existiert bereits eine Vorlage mit diesem Namen</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="58"/>
        <source>Please enter the new name</source>
        <translation>Bitte gib einen neuen Namen ein</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="90"/>
        <location filename="../ManageTemplatesWindow.py" line="73"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="73"/>
        <source>A error occurred while renaming</source>
        <translation>Während des Umbenennenens ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="83"/>
        <source>Delete {{name}}</source>
        <translation>Lösche {{name}}</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="83"/>
        <source>Are you sure you want to delete {{name}}?</source>
        <translation>Bist du sicher, dass du {{name}} löschen willst?</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="90"/>
        <source>A error occurred while deleting</source>
        <translation>Während des Löschens ist ein fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Manage templates</source>
        <translation>Vorlagen verwalten</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>OarsWidget</name>
    <message>
        <location filename="../OarsWidget.py" line="145"/>
        <location filename="../OarsWidget.py" line="140"/>
        <location filename="../OarsWidget.py" line="137"/>
        <location filename="../OarsWidget.py" line="133"/>
        <location filename="../OarsWidget.py" line="128"/>
        <location filename="../OarsWidget.py" line="122"/>
        <location filename="../OarsWidget.py" line="117"/>
        <location filename="../OarsWidget.py" line="112"/>
        <location filename="../OarsWidget.py" line="105"/>
        <location filename="../OarsWidget.py" line="100"/>
        <location filename="../OarsWidget.py" line="95"/>
        <location filename="../OarsWidget.py" line="88"/>
        <location filename="../OarsWidget.py" line="83"/>
        <location filename="../OarsWidget.py" line="77"/>
        <location filename="../OarsWidget.py" line="73"/>
        <location filename="../OarsWidget.py" line="69"/>
        <location filename="../OarsWidget.py" line="62"/>
        <location filename="../OarsWidget.py" line="57"/>
        <location filename="../OarsWidget.py" line="52"/>
        <location filename="../OarsWidget.py" line="47"/>
        <location filename="../OarsWidget.py" line="42"/>
        <location filename="../OarsWidget.py" line="37"/>
        <location filename="../OarsWidget.py" line="32"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="33"/>
        <source>Mild: Cartoon characters in unsafe situations</source>
        <translation>Milde: Zeichentrickfiguren in unsicheren Situationen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="34"/>
        <source>Moderate: Cartoon characters in aggressive conflict</source>
        <translation>Moderat: Zeichentrickfiguren in aggressivem Konflikt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="35"/>
        <source>Intense: Cartoon characters showing graphic violence</source>
        <translation>Intensiv: Zeichentrickfiguren mit grafischer Gewaltdarstellung</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="38"/>
        <source>Mild: Fantasy characters in unsafe situations</source>
        <translation>Milde: Fantasy-Figuren in unsicheren Situationen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="39"/>
        <source>Moderate: Fantasy characters in aggressive conflict</source>
        <translation>Moderat: Fantasy-Figuren in aggressivem Konflikt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="40"/>
        <source>Intense: Fantasy characters with graphic violence</source>
        <translation>Intensiv: Fantasy-Figuren mit grafischer Gewaltdarstellung</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="43"/>
        <source>Mild: Realistic characters in unsafe situations</source>
        <translation>Milde: Realistische Charakter in unsicheren Situationen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="44"/>
        <source>Moderate: Realistic characters in depictions of aggressive conflict</source>
        <translation>Moderat: Realistische Charaktere  in aggressivem Konflikt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="45"/>
        <source>Intense: Realistic characters with graphic violence</source>
        <translation>Intensiv: Realistische Charaktere mit grafischer Gewaltdarstellung</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="48"/>
        <source>Mild: Unrealistic bloodshed</source>
        <translation>Milde: Unrealistisches Blutvergießen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="49"/>
        <source>Moderate: Realistic bloodshed</source>
        <translation>Moderat: Realistische Blutspritzer</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="50"/>
        <source>Intense: Depictions of bloodshed and the mutilation of body parts</source>
        <translation>Intensiv: Darstellungen von Blutvergießen und Verstümmelung von Körperteilen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="53"/>
        <source>Intense: Rape or other violent sexual behavior</source>
        <translation>Intensiv: Vergewaltigung oder andres gewalttätiges sexuelles Verhalten</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="58"/>
        <source>Mild: Visible dead human remains</source>
        <translation>Milde: Menschlishce Leichen bleiben sichtbar liegen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="59"/>
        <source>Moderate: Dead human remains that are exposed to the elements</source>
        <translation>Moderat: Menschliche Überreste sind den Elementen ausgesetzt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="60"/>
        <source>Intense: Graphic depictions of desecration of human bodies, for example being eaten by wild animals</source>
        <translation>Intensi: Grafische Darstellungen der Schändung menschlicher Körper z. B.von Tieren gefressen werden</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="63"/>
        <source>Mild: Depictions or references to historical slavery</source>
        <translation>Milde: Darstellungen oder Verweise auf die historische Sklaverei</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="64"/>
        <source>Moderate: Depictions of modern-day slavery</source>
        <translation>Moderat: Darstellungen der modernen Sklaverei</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="65"/>
        <source>Intense: Graphic depictions of modern-day slavery</source>
        <translation>Intensiv: Grafische Darstellungen der modernen Sklaverei</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="70"/>
        <source>Mild: References to alcoholic beverages</source>
        <translation>Milde: Hinweise auf alkoholische Getränke</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="71"/>
        <source>Moderate: Use of alcoholic beverages</source>
        <translation>Moderat: Benutzung von alkoholischen Getränken</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="74"/>
        <source>Mild: References to illicit drugs</source>
        <translation>Milde: Hinweise auf illegale Drogen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="75"/>
        <source>Moderate: Use of illicit drugs</source>
        <translation>Moderat: Benutzung von illegalen Drogen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="78"/>
        <source>Mild: References to tobacco products</source>
        <translation>Milde: Anspielungen auf Tabakprodukte</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="79"/>
        <source>Moderate: Use of tobacco products</source>
        <translation>Moderat: Benutzung von Tabakprodukten</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="84"/>
        <source>Mild: Brief artistic nudity</source>
        <translation>Milde: Kurze künstlerische Nacktheit</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="85"/>
        <source>Moderate: Prolonged nudity</source>
        <translation>Moderat: Ausgedehnte Nacktheit</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="86"/>
        <source>Intense: Explicit nudity showing nipples or sexual organs</source>
        <translation>Intensiv: Explizite Nacktheit, die Brustwarzen oder Geschlechtsorgane zeigt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="89"/>
        <source>Mild: Provocative references or depictions</source>
        <translation>Milde: Provokative Hinweise oder Darstellungen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="90"/>
        <source>Moderate: Sexual references or depictions</source>
        <translation>Moderat: Sexuelle Anspielungen oder Darstellungen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="91"/>
        <source>Intense: Graphic sexual behavior</source>
        <translation>Intensiv:Graphisches Sexualverhalten</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="101"/>
        <location filename="../OarsWidget.py" line="96"/>
        <source>Mild: Mild or infrequent use of profanity e.g. &apos;Dufus&apos;</source>
        <translation>Milde: Leichte oder seltene Verwendung von Schimpfwörtern, z. B.&quot;Idiot&quot;</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="102"/>
        <location filename="../OarsWidget.py" line="97"/>
        <source>Moderate: Moderate use of profanity e.g. &apos;Shit&apos;</source>
        <translation>Moderat: Mäßige Verwendung von Schimpfwörtern, z. B. &quot;Scheiße&quot;</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="103"/>
        <location filename="../OarsWidget.py" line="98"/>
        <source>Intense: Strong or frequent use of profanity e.g. &apos;Fuck&apos;</source>
        <translation>Intensiv: Starke oder häufige Verwendung von Schimpfwörtern z.B. &quot;Fuck&quot;</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="106"/>
        <source>Mild: Negativity towards a specific group of people, e.g. ethnic jokes</source>
        <translation>Milde: Negativität gegenüber einer bestimmten Gruppe von Menschen, z. B. Witze über ethnische Gruppen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="107"/>
        <source>Moderate: Discrimation designed to cause emotional harm, e.g. racism, or homophobia</source>
        <translation>Moderat: Diskriminierung mit dem Ziel, emotionalen Schaden anzurichten, z. B. Rassismus oder Homophobie</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="108"/>
        <source>Intense: Explicit discrimination based on gender, sexuality, race or religion, e.g. genocide</source>
        <translation>Intensiv: Explizite Diskriminierung aufgrund von Geschlecht, Sexualität, Rasse oder Religion, z. B. Völkermord</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="113"/>
        <source>Mild: Product placement, e.g. billboards in a football game</source>
        <translation>Milde: Produktplatzierungen z.B. Anzeigetaffeln in einem Fußballspiel</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="114"/>
        <source>Moderate: Explicit references to specific brands or trademarked products</source>
        <translation>Moderat: Explizite Verweise auf bestimmte Marken oder geschützte Produkte</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="115"/>
        <source>Intense: Users are encouraged to purchase specific real-world items</source>
        <translation>Intensiv: Die Nutzer werden ermutigt, bestimmte Gegenstände aus der echten Welt zu kaufen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="118"/>
        <source>Mild: Gambling on random events using tokens or credits</source>
        <translation>Milde: Glücksspiel bei Zufallsereignissen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="119"/>
        <source>Moderate: Gambling using fictional money</source>
        <translation>Moderrat: Glücksspiel mit Spielgeld</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="120"/>
        <source>Intense: Gambling using real money</source>
        <translation>Intensiv: Glücksspiel mit Echtgeld</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="123"/>
        <source>Mild: Users are encouraged to donate real money, e.g. using Patreon</source>
        <translation>Milde: Nutzer werden ermutigt, Echtgeld zu spenden z.B. über Patreon</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="124"/>
        <source>Intense: Ability to spend real money in-app, e.g. buying new content or new levels</source>
        <translation>Intensiv: Die Möglichkeit, innerhalb der Software echtes Geld auszugeben z.B. der Kauf von neuen Inhalten oder Leveln</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="129"/>
        <source>Mild: User-to-user game interactions without chat functionality e.g. playing chess</source>
        <translation>Milde: Benutzerinteraktion ohne Chatmöglichkeit innerhalb des Spiels z.B. Schach spielen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="130"/>
        <source>Moderate: Moderated messaging between users</source>
        <translation>Moderat: Moderierter Nachrichtenaustauch zwischen Benutzern</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="131"/>
        <source>Intense: Uncontrolled chat functionality between users</source>
        <translation>Intensiv: Unkontrollierter Nachrichtenaustauch zwischen Benutzern</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="134"/>
        <source>Moderate: Moderated audio or video chat between users</source>
        <translation>Moderat: Moderierter Audio oder Video Chat zwischen Nutzern</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="135"/>
        <source>Intense: Uncontrolled audio or video chat between users</source>
        <translation>Intensiv: Unkontrollierter Audio und Video Chat zwischen Nutzern</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="138"/>
        <source>Intense: Sharing Twitter, Facebook or email addresses</source>
        <translation>Intensiv: Teilen von Twitter, Facebook oder der E-Mail Adresse</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="141"/>
        <source>Mild: Using any online API, e.g. a user-counter</source>
        <translation>Milde: Die Benutzung einer Online API z.B. ein Nutzerzähler</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="142"/>
        <source>Moderate: Sharing diagnostic data not identifiable to the user, e.g. profiling data</source>
        <translation>Moderat: Teilen von anonymen Diagnosedaten</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="143"/>
        <source>Intense: Sharing information identifiable to the user, e.g. crash dumps</source>
        <translation>Intensiv: Teilen von Informationen, die persönliche Daten enthalten</translation>
    </message>
    <message>
        <location filename="../OarsWidget.py" line="146"/>
        <source>Intense: Sharing physical location to other users e.g. a postal address</source>
        <translation>Intensiv: Weitergabe des physischen Standorts an andere Nutzer, z. B. eine Postadresse</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Violence</source>
        <translation>Gewalt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Cartoon Violence</source>
        <translation>Zeichentrickgewalt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as fictional characters depicted in an animated film or a comic strip which do not look human.</source>
        <translation>Definiert als fiktive Figuren, die in einem Animationsfilm oder einem Comic dargestellt werden und nicht menschlich aussehen.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include an animated worm fighting with a bird.</source>
        <translation>Dies enthält beispielsweise einen animierten Wurm, der mit einem Vogel kämpft.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Fantasy Violence</source>
        <translation>Fantasy Gewalt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as characters easily distinguishable from reality.</source>
        <translation>Definiert als Charaktere, die leicht von der Realität zu unterscheiden sind.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include human units in a turn-taking isometric game.</source>
        <translation>Dies enthält beispielsweise menschliche Einheitenin einem rundenbasierten, isometrischem Spiel.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Realistic Violence</source>
        <translation>Realistsiche Gewalt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as characters not easily distinguishable from reality.</source>
        <translation>Definiert als Charaktere, die nicht leicht von der Realität zu unterscheiden sind.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include a user shooting a gun at a human characters.</source>
        <translation>Dies enthält beispielsweise Speile, in denen auf menschliche Charaktere geschossen wird.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Bloodshed</source>
        <translation>Blutvergießen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as the killing or wounding of people.</source>
        <translation>Defniert als das töten oder verwunden von Menschen.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include a user shooting a gun at a human head, causing it to explode.</source>
        <translation>Dies enthät beispielsweise einem Nutzer, der durch einen Schuss auf einen menschlichen Kopf diesen zum explodieren bringt.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Sexual Violence</source>
        <translation>Sexuelle Gewallt</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as any unwanted sexual act or activity.</source>
        <translation>Definiert als jedweder unerwünschter sexueller Akt.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include non-consensual sex or grabbing the crotch of another user.</source>
        <translation>Dies enthält beispielsweise nicht einvernehmlichen Sex oder das Grapschen in den Schritt eines anderen Nutzers.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Violence II</source>
        <translation>Gewalt II</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Desecration</source>
        <translation>Schändung</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as the action of desecrating something, typically a human body.</source>
        <translation>Definiert als die Schändung einer Sache, in der Regel eines menschlichen Körpers.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>This is an cultural sensitivity question.</source>
        <translation>Dies ist eine kulturell sensible Frage.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Human Slavery</source>
        <translation>Meschliche Sklaverei</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as working without proper remuneration or appreciation.</source>
        <translation>Definiert als Arbeit ohne angemessene Vergütung oder Wertschätzung.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Drugs</source>
        <translation>Drogen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Alcohol</source>
        <translation>Alkohol</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include seeing a character drink a beer.</source>
        <translation>Dies enthält beispielsweise einen Charakter, der Bier trinkt.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Narcotics</source>
        <translation>Betäubungsmittel</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as an addictive drug affecting mood or behaviour that is specifically illegal in at least one country.</source>
        <translation>Definiert als eine süchtig machende Droge, die die Stimmung oder das Verhalten beeinflusst und die in mindestens einem Land ausdrücklich illegal ist.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include references to &quot;getting high&quot;</source>
        <translation>Dies enthält beispielweise Referenzen auf das &quot;High werden&quot;</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Tobacco</source>
        <translation>Tabak</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as any nicotine-rich product.</source>
        <translation>Definiert als ein beliebiges nikotinhaltiges Produkt.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include references to chewing tobacco.</source>
        <translation>Dies enthält beispielsweise Hinweise auf Kautabak.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Sex and Nudity</source>
        <translation>Sex und Nacktheit</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Nudity</source>
        <translation>Nacktheit</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as a state of undress, and in this case specifically specifically nudity likely to cause offense.</source>
        <translation>Definiert als ein unbekleideter Zustand, speziell Nacktheit, der Anstoß erregen kann.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include topless cheerleaders.</source>
        <translation>Dies enthält beispielsweise oberkörperfreie Cheerleader.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Sexual Themes</source>
        <translation>Sexuelle Themen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as in reference to a sexual act.</source>
        <translation>Definiert als eine Referenz auf einen sexuellen Akt.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include blowing a kiss to another character.</source>
        <translation>Dies enthält beispielsweise einen Character, der einen anderen Character küsst.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Profanity</source>
        <translation>Obszönität</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as blasphemous or obscene language.</source>
        <translation>Definiert als blasphemische oder obszöne Sprache.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include most curse words.</source>
        <translation>Dies enthält beispielsweise die meisten Schimpfwörter.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Humor</source>
        <translation>Humor</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as the quality of being amusing.</source>
        <translation>Definiert als die Eigenschaft, amüsant zu sein.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include in-app jokes.</source>
        <translation>Dies enthät beispielsweise Witze innerhalb der Software.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Discrimination</source>
        <translation>Diskriminierung</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as the unjust or prejudicial treatment of different categories of people, especially on the grounds of race, age, or sex.</source>
        <translation>Definiert als ungerechte oder vorurteilsbehaftete Behandlung verschiedener Arten von Menschen, insbesondere aufgrund von Rasse, Alter oder Geschlecht.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Money</source>
        <translation>Geld</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Advertising</source>
        <translation>Werbung</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as the activity of producing advertisements for commercial products or services.</source>
        <translation>Definert als das Erstellen von Werbung für kommerzielle Produkte oder Dienstleistungen.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include banners showing the Coca-Cola logo shown in a Soccer game.</source>
        <translation>Das enthält beispielweise das Zeigen des Coca-Cola Logos in einem Fußballspiel.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Gambling</source>
        <translation>Glücksspiel</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as taking a risky action in the hope of a desired result.</source>
        <translation>Definert als das vollführen einer riskaten Aktion in Hoffnung auf ein erwartetes Ergebniss.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include spinning a wheel to get in-app credits.</source>
        <translation>Dies enthält beispielweise das drehen einenes Glücksrades zum Erhalt von In-Game Währung.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>In-App Purchases</source>
        <translation>In-App Käufe</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as items or points that a user can buy for use within a virtual world to improve a character or enhance the playing experience.</source>
        <translation>Definiert als Items oder Punkte, die ein Nutzer kaufen kann, um sie in einer virtuellen Welt zu verwenden, um einen Charakter zu verbessern oder das Spielerlebnis zu verbessern.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Social</source>
        <translation>Soziales</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Online Text-only Messaging</source>
        <translation>Online Textnachrichten</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as any messaging system connected to the Internet.</source>
        <translation>Definiert als ein Nachrichtensystem, dass mit dem Internet verbunden ist.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Online Audio and Video Messaging</source>
        <translation>Online Audio und Video Nachrichten</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as any multimedia messaging system connected to the Internet.</source>
        <translation>Definiert als ein beliebiges mit dem Internet verbundene Multimedia-Nachrichtensystem.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Contact Details</source>
        <translation>Kontaktdetails</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as sharing identifiable details with other users to allow out-of-band communication.</source>
        <translation>Definiert als Austausch von privaten Informationen, die einen Kontakt außerhalb der Software erlauben.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Information Sharing</source>
        <translation>Teilen von Informationen</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as sharing information with a legal entity typically used for advertising or for sending back diagnostic data.</source>
        <translation>Definiert als Weitergabe von Informationen an eine juristische Person, die in der Regel zu Werbezwecken oder zur Rücksendung von Diagnosedaten verwendet wird.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include sending your purchasing history to Amazon.</source>
        <translation>Dies enthält beispielsweise das Senden des Einkaufsverlauf an Amazon.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Location Sharing</source>
        <translation>Teilen des Standortes</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>Defined as sharing your physical real-time location.</source>
        <translation>Defineiert als das teilen der Position in der echten Welt in Echtzeit.</translation>
    </message>
    <message>
        <location filename="../OarsWidget.ui" line="0"/>
        <source>For example, this would include uploading the GPS co-ordinates of your current location. NOTE: This does not include heuristic based location services, e.g. GeoIP and others.</source>
        <translation>Dazu gehört zum Beispiel das Hochladen der GPS-Koordinaten des  aktuellen Standorts. HINWEIS: Dies gilt nicht für heuristisch basierte Standortdienste wie z. B. GeoIP.</translation>
    </message>
</context>
<context>
    <name>RelationsWidget</name>
    <message>
        <location filename="../RelationsWidget.py" line="33"/>
        <location filename="../RelationsWidget.py" line="24"/>
        <source>Not specified</source>
        <translation>Nicht angegeben</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="25"/>
        <source>Very small screens e.g. wearables</source>
        <translation>Sehr kleine Bildschirme z.B.  Wearables</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="26"/>
        <source>Small screens e.g. phones</source>
        <translation>Kleine Bildschirme z.B. Smartphones</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="27"/>
        <source>Screens in laptops, tablets</source>
        <translation>Bildschirme in Laptops, Tablets</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="28"/>
        <source>Bigger computer monitors</source>
        <translation>Größere Computermonitore</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="29"/>
        <source>Television screens, large projected images</source>
        <translation>Fernseher, Beamer</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="34"/>
        <source>Never uses the internet, even if it’s available</source>
        <translation>Nutzt nie das Internet, selbst wenn es verfügbar ist</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="35"/>
        <source>Uses the internet only the first time the application is run</source>
        <translation>Verwendet das Internet nur bei der ersten Ausführung der Anwendung</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="36"/>
        <source>Needs internet connectivity to work</source>
        <translation>Benötigt zum Betrieb eine Internetverbindung</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="156"/>
        <location filename="../RelationsWidget.py" line="123"/>
        <source>Supported</source>
        <translation>Unterstützt</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="157"/>
        <location filename="../RelationsWidget.py" line="124"/>
        <source>Recommend</source>
        <translation>Empfohlen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.py" line="158"/>
        <location filename="../RelationsWidget.py" line="125"/>
        <source>Required</source>
        <translation>Benötigt</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.py" line="169"/>
        <location filename="../RelationsWidget.py" line="136"/>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Screen</source>
        <translation>Bildschirm</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Required maximum size</source>
        <translation>Vorgeschriebene Maximalgröße</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>You can select the maximum screen size, that is rerequired to use the Software here</source>
        <translation>Du kannst hier die vorgeschriebene maximale Bildschirmgröße, die die Software unterstützt festlegen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Device class</source>
        <translation>Geräteklasse</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Custom</source>
        <translation>Eigen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>The value in logical pixels</source>
        <translation>Anzahl der logischen Pixel</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Recommend maximum size</source>
        <translation>Empfohlenen Maximalgröße</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>You can select the maximum screen size, that is recommend to use the Software here</source>
        <translation>Du kannst hier die empfohlene maximale Bildschirmgröße, die die Software unterstützt festlegen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Recommend minimum size</source>
        <translation>Empfohlene Mindestgröße</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>You can select the screen size, that is recommend to use the Software here</source>
        <translation>Du kannst hier die empfohlene minimale Bildschirmgröße, die die Software unterstützt festlegen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Required minimum size</source>
        <translation>Vorgeschriebene Mindestgröße</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>You can select the minimum screen size, that is rerequired to use the Software here</source>
        <translation>Du kannst hier die vorgeschriebene minimale Bildschirmgröße, die die Software unterstützt festlegen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Memory</source>
        <translation>Arbeitsspeicher</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>You can define the required and recommend RAM size for the Software here</source>
        <translation>Die kannst hier die benötigete und die empfohlene Menge Arbeitsspeicher, die zur Benutzung der Software erforderlich ist, festlegen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Required:</source>
        <translation>Benötigt:</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Recommend:</source>
        <translation>Empfohlen:</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>You can define here, if the Software needs Internet access. For more information take a look at the documentation.</source>
        <translation>Wenn die Software Internetzugang benötigt, kannst du das hier angeben. Wird einen Blick in die Dokumentation für weitere Informationen.</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Supports:</source>
        <translation>Ünterstützt:</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Requires:</source>
        <translation>Benätigt:</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Bandwidth:</source>
        <translation>Bandbreite:</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Recommends:</source>
        <translation>Empfohlen:</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Mbit/s</source>
        <translation>Mbit/s</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Modalias</source>
        <translation>Modalias</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Check for specific hardware to be present via its modalias. The modalias may contain a wildcard expression.</source>
        <translation>Prüfung auf Hardware mithilfe des  modalias. Der modalias kann einen Platzhalterausdruck enthalten.</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>Hardware</source>
        <translation>Hardware</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>If the Software need special hardware e.g. a configuration tool for a special Notebook, you can define that here. The hardware is identified with its Computer Hardware ID (CHID).</source>
        <translation>Wenn die Software eine spezielle Hardwarekonfiguration benötigt zB. ein Konfigurationstool für ein Laptopmodell, kannst du das hier festlegen. Die Hardware wird über die Computer Hardware ID (CHID) identifiziert.</translation>
    </message>
    <message>
        <location filename="../RelationsWidget.ui" line="0"/>
        <source>CHID</source>
        <translation>CHID</translation>
    </message>
</context>
<context>
    <name>ReleaseImporter</name>
    <message>
        <location filename="../ReleaseImporter.py" line="123"/>
        <location filename="../ReleaseImporter.py" line="94"/>
        <location filename="../ReleaseImporter.py" line="66"/>
        <location filename="../ReleaseImporter.py" line="26"/>
        <source>Enter Repo URL</source>
        <translation>Repo URl eingeben</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="26"/>
        <source>Please Enter the URL to the GitHub Repo</source>
        <translation>Bitte gib die URL zu einem GitHub Repo ein</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="37"/>
        <source>Invalid URL</source>
        <translation>Ungültige URL</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="37"/>
        <source>Could not get the Repo and Owner from the URL</source>
        <translation>Konnte Besitzer und Name nicht aus der URL extrahieren</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="133"/>
        <location filename="../ReleaseImporter.py" line="43"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="43"/>
        <source>Something went wrong while getting releases for {{url}}</source>
        <translation>Während des Importieren der Veröffentlichungen für {{url}} ist etwas schiefgelaufen</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="49"/>
        <source>Nothing found</source>
        <translation>Nichts gefunden</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="49"/>
        <source>It looks like this Repo doesn&apos;t  have any releases</source>
        <translation>Dieses Repo hat keine Releases</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="66"/>
        <source>Please Enter the URL to the GitLab Repo</source>
        <translation>Bitte gib die URL zu einem GitLab Repo ein</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="106"/>
        <location filename="../ReleaseImporter.py" line="75"/>
        <source>Could not get Data</source>
        <translation>Konnte keine Daten bekommen</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="106"/>
        <location filename="../ReleaseImporter.py" line="75"/>
        <source>Could not get release Data for that Repo. Make sure you have the right URL.</source>
        <translation>Konnte keine Daten erhalten. Stelle sicher, dass die URL stimmt.</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="94"/>
        <source>Please Enter the URL to the Gitea Repo</source>
        <translation>Bitte gib die URL zu einem Gittea Repo ein</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="120"/>
        <source>git not found</source>
        <translation>git nicht gefunden</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="120"/>
        <source>git was not found. Make sure it is installed and in PATH.</source>
        <translation>git wurde nicht gefunden. Stelle sicher dass es installiert und im PATH ist.</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="123"/>
        <source>Please Enter the URL to the Git Repo. It is tahe URL you would use with git clone.</source>
        <translation>Bitte gib die URL zum Git Repo ein. Es ist die gleiche URL dir mit git clone benutzt wird.</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="133"/>
        <source>Could not access git repo {{url}}</source>
        <translation>Kann nicht auf das Repo {{url}} zugreifen</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="150"/>
        <source>appstreamcli not found</source>
        <translation>appstreamcli nicht gefunden</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="150"/>
        <source>appstreamcli was not found. Make sure it is installed and in PATH.</source>
        <translation>appstreamcli wurde nicht gefunden. Stelle sicher dass es installiert und im PATH ist.</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="161"/>
        <source>Import of NEWS file failed</source>
        <translation>Import der NEWS Datei fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="161"/>
        <source>An error occurred while importing the NEWS file. Make sure it has the correct format.</source>
        <translation>Während des Importierens der NEWS Datei ist ein Fehler aufgetreten. Stelle sicher, dass die Datei das korrekte Format hat.</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="180"/>
        <source>From GitHub</source>
        <translation>Von GitHub</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="181"/>
        <source>From GitLab</source>
        <translation>Von GitLab</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="182"/>
        <source>From Gitea</source>
        <translation>Von Gitea</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="183"/>
        <source>From Git Repo</source>
        <translation>Von Git Repo</translation>
    </message>
    <message>
        <location filename="../ReleaseImporter.py" line="184"/>
        <source>From NEWS file</source>
        <translation>Von News Datei</translation>
    </message>
</context>
<context>
    <name>ReleasesWindow</name>
    <message>
        <location filename="../ReleasesWindow.py" line="24"/>
        <source>Not specified</source>
        <translation>Nicht angegeben</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.py" line="25"/>
        <source>Low</source>
        <translation>Gering</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.py" line="26"/>
        <source>Medium</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.py" line="27"/>
        <source>High</source>
        <translation>Hoch</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.py" line="28"/>
        <source>Critical</source>
        <translation>Kritisch</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <location filename="../ReleasesWindow.py" line="76"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <location filename="../ReleasesWindow.py" line="80"/>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.py" line="121"/>
        <source>Edit release {{release}}</source>
        <translation>Veröffentlichung {{release}} bearbeiten</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>A URL with details about the release</source>
        <translation>Eine URL mit Details über den Release</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>Urgency:</source>
        <translation>Dringlichkeit:</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>Artifacts</source>
        <translation>Artifakte</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ReleasesWindow.ui" line="0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>ScreenshotWindow</name>
    <message>
        <location filename="../ScreenshotWindow.py" line="30"/>
        <source>No URL</source>
        <translation>Keine URL</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="30"/>
        <source>Please enter a URL</source>
        <translation>Bitte gib eine URL ein</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="33"/>
        <source>Invalid URL</source>
        <translation>Ungültige URL</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="33"/>
        <source>Please enter a valid URL</source>
        <translation>Bitte gib eine gültige URL an</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="44"/>
        <source>Can&apos;t get Image</source>
        <translation>Kan Bild nicht finden</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="44"/>
        <source>It looks like the given URL does not work</source>
        <translation>Die URL ist anscheinend ungültig</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="51"/>
        <source>Can&apos;t decode Image</source>
        <translation>Kann Bild nicht dekodieren</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="51"/>
        <source>The given Image can&apos;t be decoded</source>
        <translation>Das angebene Bild kann nicht dekodiert werden</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="95"/>
        <source>Add Screenshot</source>
        <translation>Screenshot hinzufügen</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="112"/>
        <source>Edit Screenshot</source>
        <translation>Screenshot bearbeiten</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.py" line="115"/>
        <source>If you click Preview, your Screenshot will appear here scaled by 256x256</source>
        <translation>Wenn du auf Vorschau klickst, wird hier das Bild mit einer Auflösung von 256x256 erscheinen</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>Edit the properties of your Screenshot below.</source>
        <translation>Bearbeite die Eigenschaften des Screenshots hier.</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>The direct URL to the Image</source>
        <translation>Die direkte URL zum Bild</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>Width:</source>
        <translation>Breite:</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>The width of the image in pixels</source>
        <translation>Die Breite des Bildes in Pixeln</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>Height:</source>
        <translation>Höhe:</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>The height of the image in pixels</source>
        <translation>Die Höhe des Bildes in Pixeln</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>Caption:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>A short description of the Image</source>
        <translation>Eine kurze Beschreibung des Bildes</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>Translate</source>
        <translation>Übersetzen</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ScreenshotWindow.ui" line="0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../SettingsWindow.py" line="16"/>
        <source>System language</source>
        <translation>Systemsprache</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.py" line="27"/>
        <source>Nothing</source>
        <translation>Nichts</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.py" line="28"/>
        <source>Filename</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.py" line="29"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>(needs restart)</source>
        <translation>(benötigt Neustart)</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Length of recent files list:</source>
        <translation>Länge der zuletzt geöffneten Dateien:</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Window title:</source>
        <translation>Fenstertitel:</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Ask before closing unsaved File</source>
        <translation>Vor dem schließen einer Dateien nachfragen</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Show in Title if File is edited</source>
        <translation>Zeige, wenn Datei ungespeicherte Änderungen enthält</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Add comment to Files</source>
        <translation>Füge Kommentar zu Dateien hinzu</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../SettingsWindow.ui" line="0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>TranslateWindow</name>
    <message>
        <location filename="../TranslateWindow.py" line="29"/>
        <source>Select Language</source>
        <translation>Sprache auswählen</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.ui" line="0"/>
        <location filename="../TranslateWindow.py" line="43"/>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.py" line="57"/>
        <source>No Language selected</source>
        <translation>Keine Sprache ausgewählt</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.py" line="57"/>
        <source>You had no Language selected for at least one Item</source>
        <translation>Du hast für mindestens einen Eintrag keine Sprache ausgewählt</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.py" line="63"/>
        <source>Language double</source>
        <translation>Sprache doppelt</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.py" line="63"/>
        <source>{name} appears twice or more times in the table</source>
        <translation>{name} taucht mehrmals in der Tabelle auf</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.py" line="67"/>
        <source>No Text</source>
        <translation>Kein Text</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.py" line="67"/>
        <source>The Translation for {name} has no Text</source>
        <translation>Die Übersetzung für {name} hat keinen Text</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.ui" line="0"/>
        <source>Translate</source>
        <translation>Übersetzen</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.ui" line="0"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.ui" line="0"/>
        <source>Translation</source>
        <translation>Übersetzung</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.ui" line="0"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.ui" line="0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../TranslateWindow.ui" line="0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>ValidateWindow</name>
    <message>
        <location filename="../ValidateWindow.py" line="28"/>
        <source>appstreamcli was not found</source>
        <translation>appstreamcli wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.py" line="41"/>
        <source>appstream-util was not found</source>
        <translation>appstream-util wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.py" line="50"/>
        <source>No ID</source>
        <translation>Keine ID</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.py" line="50"/>
        <source>You need to set a ID to use this feature</source>
        <translation>Du benötigst eine ID, um dieses Feature nutzen zu können</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.ui" line="0"/>
        <source>Validate</source>
        <translation>Überprüfen</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.ui" line="0"/>
        <source>Mode:</source>
        <translation>Modus:</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.ui" line="0"/>
        <source>Appstreamcli</source>
        <translation>Appstreamcli</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.ui" line="0"/>
        <source>Appstream-Util Normal</source>
        <translation>Appstream-Util Normal</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.ui" line="0"/>
        <source>Appstream-Util Relax</source>
        <translation>Appstream-Util Lasch</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.ui" line="0"/>
        <source>Appstream-Util Strict</source>
        <translation>Appstream-Util Streng</translation>
    </message>
    <message>
        <location filename="../ValidateWindow.ui" line="0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>ViewXMLWindow</name>
    <message>
        <location filename="../ViewXMLWindow.ui" line="0"/>
        <source>View XML</source>
        <translation>XML ansehen</translation>
    </message>
    <message>
        <location filename="../ViewXMLWindow.ui" line="0"/>
        <source>Preview of the AppStream file</source>
        <translation>Vorschau der AppStream Datei</translation>
    </message>
</context>
</TS>
